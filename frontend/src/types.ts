export type MessageType = {
  id: string;
  room: string;
  author: string;
  message: string;
  time: string;
};
